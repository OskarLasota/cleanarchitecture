package com.example.data.repo.home.service


import com.example.model.ProductDTO
import io.reactivex.Single
import retrofit2.http.GET

interface ProductService {

    @GET("/api/products.php")
    fun getProducts(): Single<List<ProductDTO>>

}