package com.example.cleanarchitecture.home

import com.example.model.ProductModel

interface HomeContract {
    interface View {
        fun showError(message: String)
        fun initView(data : List<ProductModel>)
    }

    interface Presenter {
        fun init()
        fun onPause()
    }
}