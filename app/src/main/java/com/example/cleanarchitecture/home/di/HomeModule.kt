package com.example.cleanarchitecture.home.di


import com.example.cleanarchitecture.home.HomeActivity
import com.example.cleanarchitecture.home.HomeContract
import com.example.cleanarchitecture.home.HomePresenter
import com.example.data.repo.home.ProductRepository
import com.example.data.repo.home.service.ProductService
import com.example.domain.home.GetProductUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.Provides


@Module(includes = [HomeModule.HomeBindings::class])
class HomeModule {



    @Provides
    fun provideApiInteractor(repository: ProductRepository) : GetProductUseCaseImpl {
        return GetProductUseCaseImpl(repository)
    }

    @Module
    interface HomeBindings {

        @Binds
        fun bindHomeView(homeActivity: HomeActivity): HomeContract.View

        @Binds
        fun bindHomePresenter(homePresenter: HomePresenter): HomeContract.Presenter
    }

}
