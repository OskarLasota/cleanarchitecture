package com.example.cleanarchitecture.home

import com.example.cleanarchitecture.common.scheduler.AppScheduler
import com.example.data.repo.home.ProductToModelMapper
import com.example.domain.home.GetProductUseCaseImpl
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HomePresenter @Inject constructor(var view : HomeContract.View, private var apiInteractor : GetProductUseCaseImpl, private val productMapper: ProductToModelMapper,
                                        private val compositeDisposable: CompositeDisposable,
                                        private val scheduler: AppScheduler
) : HomeContract.Presenter{

    override fun init() {
        compositeDisposable.add(apiInteractor.getProducts()
            .subscribeOn(scheduler.io())
            .map(productMapper)
            .observeOn(scheduler.mainThread())
            .subscribe({
                println("got result successfully")
                view.initView(it)
            },{
                println("got result failed " + it.message)

                view.showError(it.message ?: "defaultError")
            })
        )



    }

    override fun onPause() {
        println("on pause")
        compositeDisposable.clear()
    }

}